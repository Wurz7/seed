$("#buscar").click(function(){
    cargar_clima()
})

function cargar_clima(){
    var clima = new XMLHttpRequest();
    var ciudad = $("#input-ciudad").val()
    var pais = $("#input-pais").val()

    if(pais == "cl")
    {
        clima.open('GET',`http://api.wunderground.com/api/eb7a37c339cfd624/conditions/q/${pais}/${ciudad}.json`,false)
        clima.send(null)
        var datos = JSON.parse(clima.response)
        console.log(datos)
        var ciudad = datos.current_observation.display_location.full
        var temperatura = datos.current_observation.temp_c
        var icon = datos.current_observation.icon_url
        var clima = datos.current_observation.weather
        var hora = datos.current_observation.observation_time
        $("#img-principal").attr('src',icon)
        $("#icono").attr('src',icon)
        $("#temparatura").html(`${temperatura}°, ${clima} `)
        $("#ubicacion").html(`${ciudad}`)
        $("#hora").html(hora)
    }
    else
    {
    var ciudad = "Error"
    var temperatura = "Error"
    var icon = "Error"
    var clima = "Error"
    var hora = "Error"
    $("#img-principal").attr('src',icon)
    $("#icono").attr('src',icon)
    $("#temparatura").html(`${temperatura}°, ${clima} `)
    $("#ubicacion").html(`${ciudad}`)
    $("#hora").html(hora)
    }
}

$("document").ready(function(){
    $("#formulario").validate({
        errorClass:"is-invalid",
        rules:{
            username:{
                required:true
            },
            correo:{
                required:true,
                email:true
            },
            fnacimiento:{
                required:true,
                date:true,
                max:2001-08-26
            }
        },
        messages:{
            username:{
                required:"Debe ingresar su Nombre"
            },
            correo:{
                required:"Debe ingresar su Correo",
                email:"Debe ingresar un correo valido"
            },
            fnacimiento:{
                required:"Debe ingresar su Fecha de nacimineto",
                date:"Debe ser una fecha valida",
                min:"Debe ser mayor de edad para ingresar"
            }
        }        
    })
})

$("#formulario").submit(function(){
    if($("#formulario").valid()){
        return true
    }
    else
    {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<a href>Why do I have this issue?</a>'
          })
    }
})

function buscar() {
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("busca_tabla_libros");
	filter = input.value.toUpperCase();
	table = document.getElementById("tabla_libros");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
	  td = tr[i].getElementsByTagName("td")[0];
	  if (td) {
		txtValue = td.textContent || td.innerText;
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
		  tr[i].style.display = "";
		} else {
		  tr[i].style.display = "none";
		}
	  }       
	}
  }