from django import forms

from .models import Archivos

class ArchivosForm(forms.ModelForm):
    class Meta:
        model = Archivos
        fields = ('Archivo',)
