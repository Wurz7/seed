from rest_framework import serializers
from .models import Libros

class Librosserializer(serializers.ModelSerializer):
    class Meta:
        model = Libros
        fields = 'Nombre_Libro'