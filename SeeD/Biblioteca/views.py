from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as log_in, logout as log_out
from .models import Registro, Libros, Categoria_Libros, Archivos
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import user_passes_test
from django.views.generic import CreateView
from .forms import ArchivosForm
from .serializers import Librosserializer
from rest_framework import viewsets

# Create your views here.
def index(request):
    if request.POST:
        return HttpResponseRedirect(reverse("index"))
    return render(request,"index.html")

def iniciar_sesion(request):
    if request.POST:
            usuario = request.POST.get('usuario',False)
            contrasenia = request.POST.get('contrasenia',False)
            usuario = authenticate(request = request,username = usuario,password = contrasenia)
            if usuario is not None:
                log_in(request,usuario)
                return HttpResponseRedirect(reverse('index'))
    return HttpResponseRedirect(reverse('index'))

def cerrar_sesion(request):
    log_out(request)
    return HttpResponseRedirect(reverse('index'))

def registro(request):
    if request.POST:
        form = Registro(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            log_in(request, user)
            return HttpResponseRedirect(reverse('index'))
    else:
        form = Registro()
    return render(request,"registro.html",{"form":form })

@login_required(login_url='index')
def perfil(request):
    context = {'user' : request.user}
    return render(request,"Perfil.html",context)

def libros(request):
    context = {'Libro' : Libros.objects.all()}
    return render(request,"libros.html",context)

def agregar_libros(request):
    if request.POST:
        nombre = request.POST.get('Nombre_Libro',False)
        escritor = request.POST.get('Escritor_Libro',False)
        pk_categoria = request.POST["Categoria_Libro"]
        categoria = Categoria_Libros.objects.filter(pk=pk_categoria)
        libro = Libros(Nombre_Libro=nombre,Escritor_Libro=escritor,Categoria_Libro = categoria[0])
        libro.save()
        return HttpResponseRedirect(reverse('agregar_libros'))
    else:
        context = {'libro' : libros}
        context['Categoria_Libros'] = Categoria_Libros.objects.all()
        return render(request,"agregar_libros.html",context)
    return HttpResponseRedirect(reverse("agregar_libros"))

@user_passes_test(lambda u:u.is_staff, login_url=reverse_lazy('libros'))
def borrar_libro(request,id):
    try:
        libro = Libros.objects.get(id=id)
        libro.delete()
        return HttpResponseRedirect(reverse("libros"))
    except Exception:
        print("ERROR")
    return HttpResponseRedirect(reverse("libros"))

class agregar_libro(CreateView):
    model = Libros
    template_name =  'agregar_libro.html'
    fields = ('Nombre_Libro','Escritor_Libro','Editorial_Libro','Categoria_Libro')
    success_url = reverse_lazy('libros')

def editar_libro(request,id):
    try:
        libro = Libros.objects.get(id=id)
        if request.POST:
            Nombre_L = request.POST.get('Nombre_Libro',False)
            Escritor_L = request.POST.get('Escritor_Libro',False)
            Editorial_L = request.POST.get('Editorial_Libro',False)
            libro.Nombre_Libro = Nombre_L
            libro.Escritor_Libro = Escritor_L
            libro.Editorial_Libro = Editorial_L
            libro.save()
            return HttpResponseRedirect(reverse("libros"))
        context = {'libro' : libro}
        context['Categoria_Libros'] = Categoria_Libros.objects.all()
        return render(request,"editar_libro.html",context)
    except Exception:
        print("ERROR")
    return HttpResponseRedirect(reverse('libros'))

def subir_archivo(request):
    if request.method == 'POST':
        form = ArchivosForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('libros'))
    else:
        form = ArchivosForm()
    return render(request, 'subir_archivo.html', {
        'form': form
    })

class subirarchivoView(CreateView):
    model = Archivos
    form_class = ArchivosForm
    success_url = reverse_lazy('libros')
    template_name = 'subir_archivo.html'

def Lista_Archivos(request):
    context = {'Archivos' : Archivos.objects.all()}
    return render(request,"lista_archivos.html",context)

class listaarchivosView(CreateView):
    model = Archivos
    form_class = ArchivosForm
    success_url = reverse_lazy('lista_archivos')
    template_name = 'lista_archivos.html'

class api_libro(viewsets.ModelViewSet):
    queryset = Libros.objects.all()
    serializer_class = Librosserializer