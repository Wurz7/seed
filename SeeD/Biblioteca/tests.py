from django.test import TestCase

from .models import Libros,Categoria_Libros
#simularemos un cliente
from django.test import Client

class LibrosTestCase(TestCase):
    def setUp(self):
        categoria = Categoria_Libros(nombre_categoria="TERROR")
        categoria.save()
        categoria = Categoria_Libros.objects.filter(nombre_categoria="TERROR")
        libro = Libros(Nombre_Libro="Nombre_Libro",Escritor_Libro="Pepito",Editorial_Libro="Ellos",Categoria_Libro=categoria[0])
        libro.save()

    def test_busca_libro(self):
        libro = Libros.objects.filter(Nombre_Libro="Nombre_Libro")
        self.assertEqual(libro[0].Nombre_Libro,"Nombre_Libro")

    def test_crea_libro(self):
        
        categoria = Categoria_Libros.objects.filter(nombre_categoria="TERROR")
        libro = Libros(Nombre_Libro="El libro",Escritor_Libro="Pepito",Editorial_Libro="Ellos",Categoria_Libro=categoria[0])
        libro.save()
        libro = Libros(Nombre_Libro="El libro",Escritor_Libro="Pepito",Editorial_Libro="Ellos",Categoria_Libro=categoria[0])
        libro.save()
        libro = Libros(Nombre_Libro="El libro",Escritor_Libro="Pepito",Editorial_Libro="Ellos",Categoria_Libro=categoria[0])
        libro.save()
        libro = Libros(Nombre_Libro="El libro",Escritor_Libro="Pepito",Editorial_Libro="Ellos",Categoria_Libro=categoria[0])
        libro.save()
        libro = Libros(Nombre_Libro="El libro",Escritor_Libro="Pepito",Editorial_Libro="Ellos",Categoria_Libro=categoria[0])
        libro.save()
        list = Libros.objects.all()
        self.assertGreaterEqual(len(list),5)