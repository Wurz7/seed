from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class Registro(UserCreationForm):

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
        )
    def save(self, commit=True):
        user = super(Registro, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()
        return user
    
class Categoria_Libros(models.Model):
    nombre_categoria = models.CharField(max_length=150)
    def __str__(self):
        return self.nombre_categoria

class Libros(models.Model):
    Nombre_Libro = models.CharField(max_length=100)
    Escritor_Libro = models.CharField(max_length=100)
    Editorial_Libro = models.CharField(max_length=100)
    Categoria_Libro = models.ForeignKey(Categoria_Libros,on_delete=models.CASCADE)

class Archivos(models.Model):
    Archivo = models.FileField(upload_to='static/Archivos/')