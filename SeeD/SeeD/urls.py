from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path,include
from Biblioteca import views
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy
from rest_framework import routers

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('iniciar_sesion/',views.iniciar_sesion,name="iniciar_sesion"),
    path('cerrar_sesion/',views.cerrar_sesion,name="cerrar_sesion"),
    path('registro/',views.registro,name="registro"),
    path('perfil/',views.perfil,name="perfil"),
    path('agregar_libro/',views.agregar_libro,name="agregar_libro"),
    path('editar_libro/',views.editar_libro,name="editar_libro"),
    path('libros/',views.libros,name="libros"),
    path('libros/borrar/<int:id>/',views.borrar_libro,name="borrar_libro"),
    path('libros/editar/<int:id>/',views.editar_libro,name="editar_libro"),
    path('libros/agregar_libro/', views.agregar_libro.as_view(),name="agregar_libro"),
    path('password_reset/', auth_views.PasswordResetView.as_view(success_url=reverse_lazy('users:password_reset_done')), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('password_reset/confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('subir_archivo/', views.subirarchivoView.as_view(), name='subir_archivo'),
    path('lista_archivos/', views.listaarchivosView.as_view(), name='lista_archivos'),
    path('agregar_libros/', views.agregar_libros, name='agregar_libros'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)